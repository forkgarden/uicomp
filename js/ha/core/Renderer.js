"use strict";
var ha;
(function (ha) {
    var core;
    (function (core) {
        class Renderer {
            renderHtml(parent, component) {
                let div = document.createElement('div');
                let el;
                div.innerHTML = component.template;
                el = div.firstElementChild;
                component.elHtml = el;
                parent.appendChild(el);
                if (component.onRender) {
                    component.onRender();
                }
                let colls = el.querySelectorAll('*');
                let key;
                for (let i = 0; i < colls.length; i++) {
                    for (key in component) {
                        if (key.toLowerCase() == colls[i].tagName.toLowerCase()) {
                            let el = colls[i];
                            let comp = component[key];
                            if (comp && comp.render)
                                comp.render(el);
                        }
                    }
                }
            }
        }
        core.Renderer = Renderer;
    })(core = ha.core || (ha.core = {}));
})(ha || (ha = {}));
