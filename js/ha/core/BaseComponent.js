"use strict";
var ha;
(function (ha) {
    var core;
    (function (core) {
        class BaseComponent {
            constructor() {
                this._renderer = new core.Renderer();
            }
            onRender() {
            }
            render(parent) {
                this._renderer.renderHtml(parent, this);
            }
            get template() {
                return this._template;
            }
            set template(value) {
                this._template = value;
            }
            get elHtml() {
                return this._elHtml;
            }
            set elHtml(value) {
                this._elHtml = value;
            }
        }
        core.BaseComponent = BaseComponent;
    })(core = ha.core || (ha.core = {}));
})(ha || (ha = {}));
