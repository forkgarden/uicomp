class Dialog {
    private dialog:ha.comm.Dialog;
    private dialog2:ha.comm.Dialog;

    private button1:HTMLElement;
    private button2:HTMLElement;

    constructor() {
        this.dialog = new ha.comm.Dialog('body dialog on create');
        this.dialog.render(document.body);

        this.dialog2 = new ha.comm.Dialog();
        this.dialog2.render(document.body);
        this.dialog2.body = 'body dialog on render';

        this.button1 = document.body.querySelector('button#button1');
        this.button1.addEventListener('click', () => {
            this.dialog.show();
        });

        this.button2 = document.body.querySelector('button#button2');
        this.button2.addEventListener('click', () => {
            this.dialog2.show();
        })
    }

}